<?php

namespace bioshock\xlsxWrapper;

/** writes  xlsx file with specified splitting criteria
 * Class batchXlsxWrapper.php
 * @package bioshock\xlsxWrapper
 */
class batchXlsxWrapper
{

    protected $columnOrder;
    protected $headerWritten = false;
    protected $index;
    /** @var  $xlsxWrapper xlsxWrapper */
    protected $xlsxWrapper;
    private $pattern;
    private $limit;
    private $batchCount = 0;

    function __construct($columnOrder, $limit = 5000, $file_name_pattern = "output_%s.xlsx", $padding_len = 4)
    {
        $this->limit = $limit;
        $this->pattern = $file_name_pattern;
        $this->padding_len = $padding_len;
        $this->columnOrder = $columnOrder;
        $this->init();
    }

    private function init()
    {
        $this->index = 0;
        $this->batchCount++;
        $this->xlsxWrapper = new xlsxWrapper();
        $this->xlsxWrapper->setColumnOrder($this->columnOrder);

    }

    /**
     * @param $title string
     */
    function setSheetTitle($title)
    {
        $this->xlsxWrapper->setSheetTitle($title);
    }


    /**
     * @param $rowArray array
     * @throws \PHPExcel_Exception
     */
    function addRow($rowArray)
    {
        if (($this->index + 1) == $this->limit) {
            $this->save();
        }
        $this->xlsxWrapper->addRow($rowArray);
        $this->index++;
    }


    /**
     * @internal param string $file
     */
    function save()
    {
        $str = str_pad($this->batchCount, $this->padding_len, '0', STR_PAD_LEFT);
        $this->xlsxWrapper->save(sprintf($this->pattern, $str));
        $this->init();
    }


}