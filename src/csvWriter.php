<?php

namespace bioshock\xlsxWrapper;


/** writes to a csv file
 * Class csvWriter
 * @package bioshock\xlsxWrapper
 */


class csvWriter
{
    protected $first = true;

    function __construct($file)
    {
        $this->writer = \League\Csv\Writer::createFromPath($file, 'w');
        $this->writer->setOutputBOM(\League\Csv\Writer::BOM_UTF8);
    }


    function addRow($row)
    {
        if ($this->first == true) {
            $this->writer->insertOne(array_keys($row)); //headers
            $this->writer->insertOne(array_values($row));
            $this->first = false;
        } else {
            $this->writer->insertOne(array_values($row));
        }
    }

    function addMultipleRows($rows)
    {

        foreach ($rows as $row) {
            $this->addRow($row);
        }
    }

    function close()
    {
        unset($this->writer);
    }

    function __destruct()
    {
        unset($this->writer);
    }

}


