<?php

namespace bioshock\xlsxWrapper;
use PHPExcel_IOFactory;

/** reads xlsx file into an  array/associative array/json
 * Class xlsxToArray
 * @package bioshock\xlsxWrapper
 */
class xlsxToArray
{


    function __construct($file,$sheetNo=0)
    {
        $filetype = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($filetype);
        $objReader->setReadDataOnly(true);  // set this if you don't need to write
        $objPHPExcel = $objReader->load($file);
        $this->array= $objPHPExcel->getSheet($sheetNo)->toArray() ;
    }

    function  toArray(){
        return $this->array  ;
    }


    function toAssocArray($headerRowNo=0, $duplicatePrefixPattern='_%d'){


        $headers = $this->array[$headerRowNo];

        $newHeaders = array();
        $duplicateIndex = array();


        foreach ($headers as $key) {

            if(!isset($newHeaders[$key]) && !isset($duplicateIndex[$key])){
                $newHeaders[$key]  = $key ;
                $duplicateIndex[$key] = 0 ;

            }
            else
            {

                $duplicateIndex[$key] =  (int) ($duplicateIndex[$key]+1) ;

                $newHeaders[sprintf($key.$duplicatePrefixPattern, $duplicateIndex[$key] )] = sprintf($key.$duplicatePrefixPattern, $duplicateIndex[$key]) ;
                if($duplicateIndex[$key]==1)
                {
                    $this->renameArrayIndex($key,  sprintf($key.$duplicatePrefixPattern, 0), $newHeaders);
                }
            }
        }

        $output = array();

        foreach ($this->array as $k=>$value) {
            if($k>$headerRowNo){

                $output[] = array_combine($newHeaders,$value);

            }
        }

        return $output;
    }

    private function renameArrayIndex($old,$new, &$target) {

        $newArr  = array() ;
        foreach ($target as $k=>$value) {

            if($k==$old) {
                $newArr[$new] = $value ;
            }else
            {
                $newArr[$k] = $value ;
            }
        }

        $target = $newArr ;
    }

}